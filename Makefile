# www_tree is the kitchen sink:
# - builds (& typechecks) the library
# - makes a distribution tarball
# - builds (& typechecks) the examples
# - builds the documentation
# - puts it all in a nice tree
all: build_www_tree

build_sidekick:
	tsc

clean:
	rm -r dist_js                   || return 0
	rm -r sidekick_dist             || return 0
	rm -r docs                      || return 0
	rm -r examples/examples_dist_js || return 0
	rm -r www_tree                  || return 0

build_dist: build_sidekick
	mkdir -p sidekick_dist
	cp    README.md   sidekick_dist
	cp    LICENSE.txt sidekick_dist
	cp -r src_ts      sidekick_dist
	cp -r dist_js     sidekick_dist

build_docs:
	npx typedoc --out                docs \
	            --entryPointStrategy expand \
	            --name               Sidekick \
	            src_ts


serve_docs:
	cd docs && python3 -m http.server 8002


build_examples: build_dist
	cd examples && tsc


serve_examples:
	cd examples && python3 -m http.server 8001


build_everything: build_sidekick build_dist build_examples build_docs

build_www_tree: build_everything
	mkdir www_tree
	cp -r sidekick_dist www_tree
	cp -r docs          www_tree
	cp -r examples      www_tree
