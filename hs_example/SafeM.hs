module SafeM where

-------------------------------------------------------------------------------
-- Type
-------------------------------------------------------------------------------

data Safe ok_type err_type
    = Ok ok_type
    | Errs [err_type]
    deriving (Eq, Show)


-- |Example: asserts that the integer is greater than or equal to 5
ge5 :: Int -> Safe Int String
ge5 n =
    if n >= 5
    then
        -- if the integer is >=5, then return Ok with the integer
        Ok n
    else
        -- otherwise make an error message
        let msg = concat ["error: ", show n, " is less than 5"]
         in Errs [msg]


-------------------------------------------------------------------------------
-- Mapping (Functorial properties)
-------------------------------------------------------------------------------

-- |Given a pure map, and a safety-wrapped value, apply the pure map to the
-- safety-wrapped value
safe_map :: (ok_t1 -> ok_t2)     -- ^pure map
         -> Safe ok_t1 err_t     -- ^wrapped input value
         -> Safe ok_t2 err_t     -- ^output value
safe_map fun safe_value =
    case safe_value of
        Ok value ->
            Ok (fun value)
        Errs errs ->
            Errs errs


-- |Subtracts 11
sub11 :: Int -> Int
sub11 n =
    n - 11


-------------------------------------------------------------------------------
-- Binding (monadic properties)
-------------------------------------------------------------------------------

safe_bind :: (ok_t1 -> Safe ok_t2 err_t)
          -> Safe ok_t1 err_t
          -> Safe ok_t2 err_t
safe_bind fun safe_val =
    case safe_val of
        Ok val ->
            fun val
        Errs errs ->
            Errs errs
