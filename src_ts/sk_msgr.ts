/**
 * MsgR: canonical implementation of AWCP_Waellet
 *
 * AWCP_Waellet: aepp-waellet communication protocol, implements
 * Waellet behavior (from the perspective of the aepp)
 *
 * In particular, this black-boxes away some of the complexity of
 * dealing with the message queue and places it into a
 * straightforward interface
 *
 * @module
 */

import * as awcp from './awcp.js';
import * as msgq from './msgq.js';


class MsgR implements awcp.AWCP_Waellet
{
    /**
     * The keyword 'implements' does what you think: it signals the
     * typechecker that any property of AWCP_Aepp must be implemented by
     * one of the instance methods of this class.
     */

    //---------------------------------------------------------------
    // STATE
    //---------------------------------------------------------------

    msgq      : msgq.MsgQ         ;
    listening : boolean   = false ;



    //---------------------------------------------------------------
    // API
    //---------------------------------------------------------------

    /**
     * Currently we just create a message queue and tell it to listen
     *
     * In the future I might black-box away complexity like
     * "selective ignore of connection.announcePresence" jizz, but
     * for now we are just doing a dumb implementation.
     */
    constructor
        ()
    {
        // start the message que
        this.msgq = msgq.start();
        //this.msgq.listen();
    }



    /**
     * Tells the `msgq` to listen
     */
    listen
        ()
        : void
    {
        this.msgq.listen();
        this.listening = true;
    }



    /**
     * Tells the `msgq` to ignore
     */
    ignore
        ()
        : void
    {
        this.msgq.ignore();
        this.listening = false;
    }



    /**
     * waits for a connection.announcePresence message from the
     * window and then returns it to the caller
     *
     * Note that this is the only instance where the wallet sends a
     * request back to the aepp
     */
    async connection_announcePresence
        (timeout_ms : number)
        : Promise<awcp.AWCP_W2A_requ_connection_announcePresence>
    {
        let response =
            await this.msgq.rcv<awcp.AWCP_W2A_requ_connection_announcePresence>
                               ("connection.announcePresence", timeout_ms);
        return response;
    }



    /**
     * gets the open message from the caller, posts it into the window
     * message queue, waits for the response from the waellet, sends
     * the response back to the caller
     */
    async connection_open
        (open_msg   : awcp.AWCP_A2W_requ_connection_open,
         timeout_ms : number)
        : Promise<awcp.AWCP_W2A_resp_connection_open>
    {
        this.post(open_msg);
        let response =
            await this.msgq.rcv<awcp.AWCP_W2A_resp_connection_open>
                               ("connection.open", timeout_ms);
        return response;
    }



    /**
     * get the "address.subscribe" message from the caller, post it
     * into the window, wait for response back from the waellet, send
     * the response back to the user
     */
    async address_subscribe
        (a2w_msg    : awcp.AWCP_A2W_requ_address_subscribe,
         timeout_ms : number)
        : Promise<awcp.AWCP_W2A_resp_address_subscribe>
    {
        this.post(a2w_msg);
        let response =
            await this.msgq.rcv<awcp.AWCP_W2A_resp_address_subscribe>
                               ("address.subscribe", timeout_ms);
        return response;
    }



    /**
     * get the "transaction.sign" message from the caller, post it
     * into the window, wait for response back from the waellet, send
     * the response back to the user
     *
     * This is the variant where the wallet attempts to propagate the
     * transaction
     */
    async transaction_sign_yes_propagate
        (a2w_msg    : awcp.AWCP_A2W_requ_transaction_sign_yes_propagate,
         timeout_ms : number)
        : Promise<awcp.AWCP_W2A_resp_transaction_sign_yes_propagate>
    {
        this.post(a2w_msg);
        let response =
            await this.msgq.rcv<awcp.AWCP_W2A_resp_transaction_sign_yes_propagate>
                               ("transaction.sign", timeout_ms);
        return response;
    }



    /**
     * get the "transaction.sign" message from the caller, post it
     * into the window, wait for response back from the waellet, send
     * the response back to the user
     *
     * This is the variant where the wallet attempts to propagate the
     * transaction
     */
    async transaction_sign_no_propagate
        (a2w_msg    : awcp.AWCP_A2W_requ_transaction_sign_no_propagate,
         timeout_ms : number)
        : Promise<awcp.AWCP_W2A_resp_transaction_sign_no_propagate>
    {
        this.post(a2w_msg);
        let response =
            await this.msgq.rcv<awcp.AWCP_W2A_resp_transaction_sign_no_propagate>
                               ("transaction.sign", timeout_ms);
        //console.log('response:', response);
        return response;
    }


    //---------------------------------------------------------------
    // INTERNALS
    //---------------------------------------------------------------

    /**
     * @internal
     *
     * This black-boxes away "post a message to the window".
     *
     * In the future, we may have this generalize to other possible
     * interfaces (rather than just `window`) and/or have the
     * canonical origin argument specified in the second argument
     * (rather than just `"*"`)
     */
    post
        (msg: any)
        : void
    {
        window.postMessage(msg, "*");
    }
}


/**
 * Start a new messager
 */
function
start
    ()
    : MsgR
{
    return new MsgR();
}

export type {
    MsgR
};

export {
    start
};

// BACK: propagate timeout parameter, think about how to do awcp
// error logic. probably the right approach is to make an internal
// function here that black-boxes away "rcv" and says "if it has the
// property error....
//
// maybe that should be done in the skylight... hmm because this
// should just be a naked "send shit to the wallet and here's what
// the wallet sent back."
//
// I think the more low-level control I can hand to Craig, the better
//
// I can't eliminate the pain, all I can do is give him the tools to
// handle it
//
// That's what TypeScript does well. It can't eliminate the pain of
// JavaScript, it just gives us tools to handle it.
