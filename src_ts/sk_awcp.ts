/**
 * # AWCP: the Aepp-Waellet Communication Protocol
 *
 * The idea here is to use TypeScript's type system to express the
 * shape of messages (really events) passed between the Aepp and the
 * Waellet.  The **Aepp** is what you are writing, and the
 * **Waellet** is what sidekick talks to.
 *
 * It might be useful to reference:
 *
 *  - JSON RPC 2.0 definition: https://www.jsonrpc.org/specification
 *  - Typescript generics: https://www.typescriptlang.org/docs/handbook/2/generics.html
 *
 * The message protocol works by both the Aepp and the Waellet
 * posting events into the `window`'s global event queue.
 *
 * From sidekick's perspective, there are 4 "parameters" in such a
 * message
 *
 * 1. Whether the message is for the Aepp or for the Waellet
 * 2. Whether the message is a request or a response
 * 3. The `method` of the message (a string)
 * 4. The actual data contained in the message. For "requests" this
 *    parameter is called `params`, and for "responses" this
 *    parameter is called `result`. The nomenclature of the compound
 *    types reflects this.
 *
 * Remember that types are fake. There is nothing crazy going on
 * here. All we are doing is using weird looking notation to express
 * in a very straightforward manner how the data you have to deal
 * with is going to be shaped.
 *
 * From your perspective as a developer using sidekick in your
 * application, you will probably never deal with the "request"
 * types, only with the "result" types that top-level functions might
 * return.
 *
 * We will work outside-in. If we combine the first two parameters,
 * there are 4 possible types of messages:
 *
 * 1. An Aepp-to-Waellet request. This is the generic type
 *    `Window_A2W_Requ`
 * 2. A Waellet-to-Aepp response. This is the generic type
 *    `Window_W2A_Resp`
 * 3. A Waellet-to-Aepp request. This is the generic type
 *    `Window_W2A_Requ`
 * 4. An Aepp-to-Waellet response. This does not occur in practice.
 *
 * A "generic type" is a function at the level of types. Meaning it
 * is a transformation that takes one or more types as inputs (in our
 * case, two, for the two remaining parameters), and produces a
 * single type as an output.
 *
 * Here are the types:
 *
 * ```typescript
 * export type Window_A2W_Requ<method_s, params_t> =
 *     {type : "to_waellet",
 *      data : RpcRequ<method_s, params_t>};
 *
 * export type Window_W2A_Resp<method_s, result_t> =
 *     {type : "to_aepp",
 *      data : RpcResp<method_s, result_t>};
 *
 * export type Window_W2A_Requ<method_s, params_t> =
 *     {type : "to_aepp",
 *      data : RpcRequ<method_s, params_t>};
 *```
 *
 * The `method_s` is meant to be a string, which corresponds to the
 * third parameter listed above.  Remember that in TypeScript,
 * literal values are valid types.  The `params_t` or `result_t` is
 * meant to be a type, which corresponds to the actual meat of the
 * message, the fourth parameter listed above.
 *
 * For example, let's look at the messages sent between the Aepp and
 * the Waellet when you send the Waellet a transaction that you want
 * the Waellet to sign, but not propagate into the network.
 *
 * ```typescript
 * // This is the data (4th parameter) that your Skylight sends to
 * // the Waellet whenever you ask the Waellet to sign a transaction.
 * //
 * // Note: the `returnSigned` attribute in this object determines
 * // whether or not the Waellet attempts to propagate the
 * // transaction after signing it. The fact that we can use literal
 * // values in types allows us to express the distinct behaviors
 * // within the type system, and to catch potential crossups at
 * // compile time.
 * export type AWCP_A2W_params_transaction_sign_no_propagate =
 *     {tx           : string,
 *      returnSigned : true,
 *      networkId    : string};
 *
 * // This is the data (4th parameter) that the wallet sends back
 * // after signing the transaction.
 * //
 * // This is what will be returned back to you in a top-level
 * // function call.
 * export type AWCP_W2A_result_transaction_sign_no_propagate =
 *     {signedTransaction: string};
 *
 * // This is the shape of data that Skylight actually posts in the
 * // window event queue
 * export type AWCP_A2W_requ_transaction_sign_no_propagate =
 *     Window_A2W_Requ<"transaction.sign",
 *                     AWCP_A2W_params_transaction_sign_no_propagate>;
 *
 * // This is the shape of data that the Waellet posts in the window
 * // event queue in response
 * export type AWCP_W2A_resp_transaction_sign_no_propagate =
 *     Window_W2A_Resp<"transaction.sign",
 *                      AWCP_W2A_result_transaction_sign_no_propagate>;
 * ```
 *
 *
 * This module uses types to define the expected behavior of the
 * waellet. There are no functions here.  Specifically, this module
 * defines an `interface` called `AWCP_Waellet`, which is
 * `implement`ed by the `MsgR` class.
 *
 * The idea there is for Skylight to treat the waellet like a black
 * box. You send it one of the request types, it sends you back the
 * corresponding response type. How it goes about doing that is
 * George's problem.
 *
 * The exception to this idiom is the manner in which the Waellet
 * announces that it exists
 *
 * @module
 */

import * as safem from '../safem.js';

//-------------------------------------------------------------------
// GENERIC JSON RPC 2.0 TYPES
//-------------------------------------------------------------------

/**
 * RPC request type
 */
export type RpcRequ<method_s, params_t> =
     {jsonrpc : "2.0",
      method  : method_s,
      params  : params_t,
      id      : number};


// Ah
// https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/rpc/RpcClient.ts#L12-L22
// ok
//
// hmm
//
// so the way it works is
//
// ok, so the request we send out is constant, and sometimes we get sent a
// request by virtue of the connection.announcePresence thing
//
// there's also more rpc methods in the sdk than what is covered here, so I
// need to go investigate those, because i might want to cover them, probably
// do, and it's possible, even likely that the idiom that is going on over
// there will change the structure over here
//
// so basically what I'm seeing
//
// let me copy that code in right here
//
//      interface JsonRpcResponse {
//        jsonrpc: '2.0';
//        id: number;
//        method: string;
//        result?: any;
//        error?: {
//          code: number;
//          message: string;
//          data?: any;
//        };
//      }
//
// you see, we get sent back a message with, whatever, the id thing, we should
// maybe think about making sure we match _ids_, rather than _methods_, in
// block-on-raseev.
//
// anyway, the only way to differentiate the two is by the presence/absence of
// the result and error fields. and so perhaps the safe monad idiom i came up
// with is overengineered... on the other hand i really like it conceptually.
// hmm...   ...   hmm.... ok well i think what we maybe need is some sort of
// wrapper function.. anywell... ok, we also have our own type of error that
// can occur in these situations, which is timeout errors. so it is true that
// we need our own positive error handling idiom... so basically, hmm... maybe
// it should be the case that the error handling thing... ok, so what there
// should be is the positive class of enumerated errors that can occur, and
// that there must be a way to identify each distinct error. so for instance,
// what ok... i suppose I just need to think about this
//
// and I need to write out some code, but this is the compression/decompression
// cycle. so basically we send out a request, get a response back. ah ok, no,
// we send out a request, we *possibly* get a response back, there's a safety
// layer there, which is we possibly have a timeout. again, using our monadic
// proceeding thing, we then, assuming we got something back from the browser,
// do an error-checking step, which further case-splits what we got back into
// ok or error, depending on if the result field is present
//
// ok, so i need to think more carefully about the errors that could occur...
// ok, so... hmm
//
// well something that's easy to do is logging
//
// ah but this is stupid because logging *must* be asynchronous, so we
// basically leak the Promise monad everywhere we do logging. ok, that's not
// awful, just annoying. Speaking of annoying, i just dropped a little piece of
// plastic in my keyboard by accident and now it's like a guitar pick stuck
// inside an acoustic guitar, getting it out is going to be a nightmare. thanks
// obama. anyway. so logging. logging logging logging. ok, so what we want, we
// definitely want a notion of log level... ok... so log level, there will i
// guess be the same as for the `console`: `debug`, `info`, `warning`, and
// `error`... ok.
//
// ah, and I need to write some tests
//
// i think for the tests, what i will do is develop jaeck russell... i should
// put all this on github. this is only on gitlab because I don't have my
// github password stored on this machine and i'm too lazy to go get it from
// the laptop. but should do that
//
// anyway
//
// what's going on... so we're thinking about two threads, well three threads
// really: this monad idiom, which i think just need some mental decompression
// on, logging, and then testing/debugging
//
// ok, so jaeck russell, so with logging, i guess the connection between the
// two is i want the log to be so explicit that we can replay any transaction
// or handshake or what not back, given the log. so... ok, so basically what
// this is defining is .... ahhhh ok... so we can in a sense define jaeck
// russell to start with, or write it to start with, as just a tool that can
// play the role of the wallet in replaying the attack... ok, so maybe I want
// to actually start, instead of writing it as a javascript or typescript
// extension or whatever, actually maybe I want to start by writing it as a
// Haskell or Erlang or Ruby script... well no... hmm. ok. this is an
// interesting thought. need to do more thinking
//
// i think what i should do, ok, what should i do, so what really do we need to
// do:
//
// - logging
//
// oh, i forgot to mention, ok, so... how do i phrase this... ok... hmm... well
// so what we want is for the person using sidekick, the developer, to be able
// to specify a logging endpoint, so basically, say for aegora, we want
// sidekick to send, as post requests, everything it's doing to some endpoint
// that we set up, so that if something goes wrong, we can look at it and see
// what happened... ok
//
// what i should set up jaeck russell as, for now, is a thing that can replay
// our side of the story...
//
// i can kind of see now why people use node. node is maybe not the greatest
// idea in the world (why would you *ever* use js when you don't have to?).
// however, for things like this, where you want to experiment with javascript
// in a more controlled and less sort of... what's the word, less quicksandy
// setting than the browser, ok
//
// so rough model:
//
// - we have our safety monad which I kind of like. it perhaps needs tweaking
//   so that the developer can `switch` on what type of error happened... not
//   sure... think about that, maybe try it out... the problem i see is that
//   this invites a tempatation to make it too dynamic and too oopy, and i
//   really think the current monad idiom, although conceptually nice, is too
//   dynamic
// - we need to have logging that is so explicit that we can replay the other
//   half of the message exchange
// - we will start developing jaeck russell as a tool that can replay the other
//   half of the exchange, and then gradually make it into a real wallet
//
// ok
//
// - i also need to go read through the aeternity crypto js libraries and see
//   how to use them
//
// what's also just occurred to me is that jaeck russell and sidekick must
// share a common logging idiom... hmm... i might have to bite the npm bullet
// after all if the packaging situation gets to be complex enough... i was
// hoping to have all of this self-contained. there's a temptation i have to
// write my own packaging system (i.e. copy zx) because NPM bad, but also when
// in rome
//
// alright, so
//
// - safety monad, maybe think about enumerated and identifiable positive errors
// - need a more constrained type for the RPC error codes. maybe do something
//   stupid like
//
//      type ERROR_CODE_RpcSuchAndSuchError = 4;
//      type ERROR_CODE_RpcSoAndSoError     = 5;
//      ...
//      type ERROR_CODE_Rpc = ERROR_CODE_RpcSuchAndSuchError
//                          | ERROR_CODE_RpcSoAndSoError
//                          ...;
//
//   not sure
//
// - need to build the logging stuff
// - need to build jaeck russell as the replayer, figure out a way to do that,
//   ideally from the command line
// - ok
// - really, gotta keep in mind, I'm developing a *developer toolchain*, and so
//   my overall goal here is to make the *overall* experience of developing for
//   aeternity as pleasant as possible
//
// alright, i really have to pee, i've been sitting here reading js for 2
// hours.
//
// ah, the logger needs timestamps, because time delays are a major factor
//
// hmm

/**
 * RPC respsonse type
 *
 * Strictly speaking, this violates the JSON RPC 2.0 spec, because
 * the "method" field is not required. However, it is present in
 * practice so /shrug/
 */
export type RpcResp<method_s, result_t> =
    {jsonrpc : "2.0",
     id      : number,
     method  : method_s,
     result  : result_t};



//-------------------------------------------------------------------
// GENERIC WINDOW MESSAGE TYPES
//-------------------------------------------------------------------

/**
 * aepp-to-waellet request
 */
export type Window_A2W_Requ<method_s, params_t> =
    {type : "to_waellet",
     data : RpcRequ<method_s, params_t>};



/**
 * waellet-to-aepp response:
 */
export type Window_W2A_Resp<method_s, result_t> =
    {type : "to_aepp",
     data : RpcResp<method_s, result_t>};



/**
 * waellet-to-aepp request (ONLY used for
 * `connection.announcePresence`)
 */
export type Window_W2A_Requ<method_s, params_t> =
    {type : "to_aepp",
     data : RpcRequ<method_s, params_t>};



//-------------------------------------------------------------------
// AEPP-WALLET COMMUNICATION PROTOCOL RPC ERROR TYPES
//
// From
// https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts
//-------------------------------------------------------------------

/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export const ERROR_CODE_RpcInvalidTransactionError  : number =      2;
/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export const ERROR_CODE_RpcBroadcastError           : number =      3;
/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export const ERROR_CODE_RpcRejectedByUserError      : number =      4;
/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export const ERROR_CODE_RpcUnsupportedProtocolError : number =      5;
/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export const ERROR_CODE_RpcConnectionDenyError      : number =      9;
/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export const ERROR_CODE_RpcNotAuthorizeError        : number =     10;
/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export const ERROR_CODE_RpcPermissionDenyError      : number =     11;
/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export const ERROR_CODE_RpcInternalError            : number =     12;
/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export const ERROR_CODE_RpcMethodNotFoundError      : number = -32601;

/** From https://github.com/aeternity/aepp-sdk-js/blob/9f23c57f68c1fba3dabfb3401a8b9d4d75c9776a/src/aepp-wallet-communication/schema.ts */
export type RpcError =
    {code    : number,
     message : string,
     data?   : any};



//-------------------------------------------------------------------
// AEPP-WALLET COMMUNICATION PROTOCOL SUCCESS TYPES
//
// Arranged in the order that they appear in a typical "let's connect
// to Superhero" situation.
//-------------------------------------------------------------------

//-------------------------------------------------------------------
// connection.announcePresence
//
// Announce Presence is its own little bag of dicks isn't it?
//
// It's the only time a request is sent from the waellet to the aepp
//-------------------------------------------------------------------

export type AWCP_W2A_params_connection_announcePresence =
    {id     : string,
     name   : string,
     origin : string,
     type   : "window" | "extension"};



export type AWCP_W2A_requ_connection_announcePresence =
    Window_W2A_Requ<"connection.announcePresence",
                    AWCP_W2A_params_connection_announcePresence>;




//-------------------------------------------------------------------
// connection.open
//-------------------------------------------------------------------

/**
 * `connection.open` aepp-to-waellet message parameters
 *
 * NOTE(dak): `networkId` appears by experimentation to be optional.
 * This may be a source of bugs in the future if it turns out to not
 * be optional
 */
export type AWCP_A2W_params_connection_open =
    {name       : string,
     version    : number,
     networkId? : string};



/**
 * `connection.open` wallet-to-aepp result
 * happens to be the same data we get spammed with for
 * `connection.announcePresence`
 */
export type AWCP_W2A_result_connection_open =
    AWCP_W2A_params_connection_announcePresence;



export type AWCP_A2W_requ_connection_open =
    Window_A2W_Requ<"connection.open",
                    AWCP_A2W_params_connection_open>;



export type AWCP_W2A_resp_connection_open =
    Window_W2A_Resp<"connection.open",
                    AWCP_W2A_result_connection_open>;



//-------------------------------------------------------------------
// address.subscribe
//-------------------------------------------------------------------

/**
 * `address.subscribe` messages
 */
export type AWCP_A2W_params_address_subscribe =
    {type  : "subscribe",
     value : "connected"};



export type AWCP_W2A_result_address_subscribe =
    {subscription : Array<string>,
     address      : {current   : object,
                     connected : object}};



export type AWCP_A2W_requ_address_subscribe =
    Window_A2W_Requ<"address.subscribe",
                    AWCP_A2W_params_address_subscribe>;



export type AWCP_W2A_resp_address_subscribe =
    Window_W2A_Resp<"address.subscribe",
                    AWCP_W2A_result_address_subscribe>;



//-------------------------------------------------------------------
// transaction.sign (propagate)
//
// Two different types: semantically, whether or not the wallet
// should propagate the message, or just return the signed
// transaction
//
// This returnSigned parameter apparently controls whether Superhero
// attempts to send the transaction into the blockchain (returnSigned
// = false), or simply signs the transaction and sends it back to you
// (returnSigned = true)
//-------------------------------------------------------------------


export type AWCP_A2W_params_transaction_sign_yes_propagate =
    {tx           : string,
     returnSigned : false,
     networkId    : string};



/**
 * this is when returnSigned: false
 * the wallet attempts to propagate the transaction
 */
export type AWCP_W2A_result_transaction_sign_yes_propagate =
    {transactionHash: {blockHash   : string,
                       blockHeight : number,
                       hash        : string,
                       signatures  : Array<string>,
                       tx          : {amount      : number,
                                      fee         : number,
                                      nonce       : number,
                                      payload     : string,
                                      recipientId : string,
                                      senderId    : string,
                                      type        : string,
                                      version     : number},
                       rawTx       : string}};



export type AWCP_A2W_requ_transaction_sign_yes_propagate =
    Window_A2W_Requ<"transaction.sign",
                    AWCP_A2W_params_transaction_sign_yes_propagate>;



export type AWCP_W2A_resp_transaction_sign_yes_propagate =
    Window_W2A_Resp<"transaction.sign",
                     AWCP_W2A_result_transaction_sign_yes_propagate>;




//-------------------------------------------------------------------
// transaction.sign (do not propagate)
//
// Two different types: semantically, whether or not the wallet
// should propagate the message, or just return the signed
// transaction
//
// This returnSigned parameter apparently controls whether Superhero
// attempts to send the transaction into the blockchain (returnSigned
// = false), or simply signs the transaction and sends it back to you
// (returnSigned = true)
//-------------------------------------------------------------------

export type AWCP_A2W_params_transaction_sign_no_propagate =
    {tx           : string,
     returnSigned : true,
     networkId    : string};



/**
 * this is when `returnSigned: true`
 * the server or sidekick attempts to propagate the transaction
 */
export type AWCP_W2A_result_transaction_sign_no_propagate =
    {signedTransaction: string};



export type AWCP_A2W_requ_transaction_sign_no_propagate =
    Window_A2W_Requ<"transaction.sign",
                    AWCP_A2W_params_transaction_sign_no_propagate>;



export type AWCP_W2A_resp_transaction_sign_no_propagate =
    Window_W2A_Resp<"transaction.sign",
                     AWCP_W2A_result_transaction_sign_no_propagate>;



/**
 * # AWCP Waellet `interface`
 *
 * A bit confusing, this lives *in the Aepp* but plays the role of
 * the Waellet *from the perspective of the Aepp*. So from the Aepp
 * code's perspective, this is the Waellet.
 *
 * That's the idea (thank you Craig for clearing this idea up for me)
 *
 * The purpose of the `interface` is to enumerate the functions and
 * their types which you can safely expect the "Waellet" to have. By
 * "Waellet" I mean an instance of the `Msgr` class, which *is* the
 * Waellet from the perspective of a Skylight.
 *
 * Basically, this is the interface that a Skylight has to talk to
 * the "Waellet".
 *
 * Note: parameter names are required in TypeScript for some reason,
 * hence all the underscores
 */

export interface AWCP_Waellet
{
    connection_announcePresence    : (                                                   timeout_ms : number ) => Promise< AWCP_W2A_requ_connection_announcePresence    >;
    connection_open                : ( _: AWCP_A2W_requ_connection_open                , timeout_ms : number ) => Promise< AWCP_W2A_resp_connection_open                >;
    address_subscribe              : ( _: AWCP_A2W_requ_address_subscribe              , timeout_ms : number ) => Promise< AWCP_W2A_resp_address_subscribe              >;
    transaction_sign_yes_propagate : ( _: AWCP_A2W_requ_transaction_sign_yes_propagate , timeout_ms : number ) => Promise< AWCP_W2A_resp_transaction_sign_yes_propagate >;
    transaction_sign_no_propagate  : ( _: AWCP_A2W_requ_transaction_sign_no_propagate  , timeout_ms : number ) => Promise< AWCP_W2A_resp_transaction_sign_no_propagate  >;
};
